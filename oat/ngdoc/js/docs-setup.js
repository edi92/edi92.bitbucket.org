NG_DOCS={
  "sections": {
    "api": "API Documentation"
  },
  "pages": [
    {
      "section": "api",
      "id": "oatApp.authInterceptorService",
      "shortName": "oatApp.authInterceptorService",
      "type": "service",
      "moduleName": "oatApp",
      "shortDescription": "Interceptor for HTTP Requests.",
      "keywords": "$location $q $sessionstorage api authinterceptorservice growl http interceptor oatapp requests service"
    },
    {
      "section": "api",
      "id": "oatApp.controller:DeviceActionController",
      "shortName": "DeviceActionController",
      "type": "controller",
      "moduleName": "oatApp",
      "shortDescription": "Controller for device action section.",
      "keywords": "$rootscope $scope action alarmondevice api backupcontacts callback controller device device_cmds deviceservice encryptdevice events growl locatedevicewithgps lockdevice method oatapp resetdevice resetpassword sendremoveconfirmation"
    },
    {
      "section": "api",
      "id": "oatApp.controller:DeviceController",
      "shortName": "DeviceController",
      "type": "controller",
      "moduleName": "oatApp",
      "shortDescription": "Controller for login page.",
      "keywords": "$interval $rootscope $routeparams $scope $translate api controller current device device_cmds deviceservice events login method oatapp preferences refreshes setbrightness togglebt togglecamera togglegps togglesync togglewifi"
    },
    {
      "section": "api",
      "id": "oatApp.controller:ForgotPwController",
      "shortName": "ForgotPwController",
      "type": "controller",
      "moduleName": "oatApp",
      "shortDescription": "Controller for forgot password modal.",
      "keywords": "$modalinstance $scope api cancel case close controller error forgot forgotpassword growl growls method modal oatapp password requests reset success userservice"
    },
    {
      "section": "api",
      "id": "oatApp.controller:LoginController",
      "shortName": "LoginController",
      "type": "controller",
      "moduleName": "oatApp",
      "shortDescription": "Controller for login page.",
      "keywords": "$location $modal $rootscope $routeparams $scope $translate alert api controller dashboard events fails login method modal oatapp openforgotpasswordmodal opens password performs redirects reset user userservice"
    },
    {
      "section": "api",
      "id": "oatApp.controller:MasterCtrl",
      "shortName": "MasterCtrl",
      "type": "controller",
      "moduleName": "oatApp",
      "shortDescription": "Main controller included in body tag.",
      "keywords": "$cookiestore $location $rootscope $scope api body controller included main method oatapp tag togglesidebar"
    },
    {
      "section": "api",
      "id": "oatApp.controller:NavController",
      "shortName": "NavController",
      "type": "controller",
      "moduleName": "oatApp",
      "shortDescription": "Controller for header navigation.",
      "keywords": "$location $modal $rootscope $scope add api broadcasts controller current device events growl header login logout method modal navigation oatapp openaddusermodal opens performs redirects selectdevice selection switches user users userservice"
    },
    {
      "section": "api",
      "id": "oatApp.controller:UserController",
      "shortName": "UserController",
      "type": "controller",
      "moduleName": "oatApp",
      "shortDescription": "Controller for adding a new user modal.",
      "keywords": "$scope adding adds adduser api cancel case closes controller dismiss error growl instance method modal modalinstance oatapp succeeds user userservice"
    },
    {
      "section": "api",
      "id": "oatApp.deviceService",
      "shortName": "oatApp.deviceService",
      "type": "service",
      "moduleName": "oatApp",
      "shortDescription": "Service for communicating with the OAT REST backend.",
      "keywords": "$http $location $q additional aid android api backend cno command communicating config confirmation data device deviceref deviceservice getdevice method number oat oatapp parameters processing promise reference removing requests rest sendcommand sendremoveconfirmation sends service"
    },
    {
      "section": "api",
      "id": "oatApp.userService",
      "shortName": "oatApp.userService",
      "type": "service",
      "moduleName": "oatApp",
      "shortDescription": "Service for communicating with the OAT REST backend.",
      "keywords": "$http $location $q $rootscope $sessionstorage address adduser api authentication backend base64 communicating config configuration current e-mail fillauthdata forgotpassword getdevice login logindata logout method newuser oat oatapp object password promise property rest service token uid unique user userservice whoami"
    }
  ],
  "apis": {
    "api": true
  },
  "html5Mode": false,
  "editExample": true,
  "startPage": "/api",
  "scripts": [
    "angular.min.js"
  ]
};