<!doctype html>
<html lang="en" ng-app="OAT">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <base href="<?php echo str_replace('/index.php', '/', $_SERVER['SCRIPT_NAME']); ?>" />

        <title>OwnAntiTheft</title>
        <!-- STYLES -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/rdash.min.css">
        <link rel="stylesheet" type="text/css" href="css/leaflet.css">
        <link rel="stylesheet" type="text/css" href="css/angular-growl.min.css">
        <link rel="stylesheet" type="text/css" href="css/oat.css">
        <!-- SCRIPTS -->
        <script type="text/javascript" src="js/lib/leaflet.js"></script>
        <script type="text/javascript" src="js/lib/angular.min.js"></script>
        <script type="text/javascript" src="js/lib/ui-bootstrap-tpls-0.13.3.min.js"></script>
        <script type="text/javascript" src="js/lib/angular-route.min.js"></script>
        <script type="text/javascript" src="js/lib/angular-cookies.min.js"></script>
        <script type="text/javascript" src="js/lib/ng-storage.min.js"></script>
        <script type="text/javascript" src="js/lib/angular-leaflet-directive.min.js"></script>
        <script type="text/javascript" src="js/lib/angular-growl.min.js"></script>
        <script type="text/javascript" src="js/lib/angular-translate.min.js"></script>
        <!-- Custom Scripts -->
        <script type="text/javascript" src="js/module.js"></script>
        <script type="text/javascript" src="js/routes.js"></script>
        <script type="text/javascript" src="js/translation.js"></script>
        <script type="text/javascript" src="js/lib/base64.js"></script>
        <script type="text/javascript" src="js/services/user-service.js"></script>
        <script type="text/javascript" src="js/services/device-service.js"></script>
        <script type="text/javascript" src="js/services/auth-interceptor.js"></script>
        <script type="text/javascript" src="js/controller/master-ctrl.js"></script>
        <script type="text/javascript" src="js/controller/login-ctrl.js"></script>
        <script type="text/javascript" src="js/controller/user-ctrl.js"></script>
        <script type="text/javascript" src="js/controller/nav-ctrl.js"></script>
        <script type="text/javascript" src="js/controller/map-ctrl.js"></script>
        <script type="text/javascript" src="js/controller/device-ctrl.js"></script>
        <script type="text/javascript" src="js/controller/device-action-ctrl.js"></script>
        <script type="text/javascript" src="js/controller/forgot-pw-ctrl.js"></script>
        <script type="text/javascript" src="js/directives/loading.js"></script>
        <script type="text/javascript" src="js/directives/widget.js"></script>
        <script type="text/javascript" src="js/directives/widget-body.js"></script>
        <script type="text/javascript" src="js/directives/widget-header.js"></script>
        <script type="text/javascript" src="js/directives/widget-footer.js"></script>
        <script type="text/javascript" src="js/directives/oat-directives.js"></script>
    </head>
    <body ng-controller="MasterCtrl">
        <div growl></div>
        <div id="page-wrapper" ng-class="{'open': toggle}" ng-cloak>

            <!-- Sidebar -->
            <div id="sidebar-wrapper">
                <ul class="sidebar">
                    <li class="sidebar-main">
                        <a ng-click="toggleSidebar()"> OwnAntiTheft <span class="menu-icon material-icons md-24">swap_horiz</span> </a>
                    </li>
                    <li class="sidebar-title">
                        <span>{{'NAV_TITLE' | translate}}</span>
                    </li>
                    <li class="sidebar-list">
                        <a href="#/">{{'NAV_DASHBOARD' | translate}} <span class="menu-icon material-icons md-24">dashboard</span></a>
                    </li>
                    <li class="sidebar-list">
                        <a href="#/sms-commands">{{'NAV_SMS' | translate}} <span class="menu-icon material-icons md-24">textsms</span></a>
                    </li>
                </ul>
                <div class="sidebar-footer">
                    <div class="col-xs-12">
                        <a href="https://bitbucket.org/edi92/oat/overview" target="_blank" translate="NAV_ABOUT"></a>
                    </div>
                </div>
            </div>
            <!-- End Sidebar -->

            <div id="content-wrapper">
                <div class="page-content">

                    <!-- Header Bar -->
                    <div class="row header" ng-controller="NavController">
                        <div class="col-xs-12">
                            <div class="user pull-right">
                                <div class="item" dropdown ng-show="authentication.isAuth">
                                    <a href="#" dropdown-toggle> <em class="material-icons md-dark md-48">person</em> </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="dropdown-header">
                                            {{authentication.email}}
                                        </li>
                                        <li class="divider"></li>
                                        <li class="link">
                                            <a href ng-click="logout()" translate="NAV_LOGOUT"></a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="item" dropdown ng-show="authentication.isAuth">
                                    <a href="#" dropdown-toggle><em class="material-icons md-dark md-36">smartphone</em></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="dropdown-header" translate="NAV_DEVICES"></li>
                                        <li class="divider"></li>
                                        <li ng-repeat="device in authentication.devices">
                                            <a href ng-click="selectDevice(device)">{{device.name}}</a>
                                        </li>
                                    </ul>
                                </div>

                                <div class="item" ng-show="authentication.isAuth && authentication.isAdmin">
                                    <a href ng-click="openAddUserModal()"> <em class="material-icons md-dark md-48">person_add</em> </a>
                                </div>
                            </div>
                            <div class="meta">
                                <div class="page" ng-show="currentDevice">
                                    {{currentDevice.name}}
                                </div>
                                <div class="page" ng-show="!currentDevice">
                                    OAT
                                </div>
                                <div class="breadcrumb-links">
                                    Home / {{pageTitle}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Header Bar -->

                    <!-- Main Content -->
                    <div ng-view></div>

                </div><!-- End Page Content -->
            </div><!-- End Content Wrapper -->
        </div><!-- End Page Wrapper -->
    </body>
</html>
