<?php
/**
 * Utility class to build simple error responses.
 */
class ErrorResponder {

    private $app;

    public function __construct() {
        $this -> app = \Slim\Slim::getInstance();
    }

    /**
     * Creates a simple error response with HTTP code 401
     *
     * @param string $message Error message.
     * @param array $errors Array of errors occurred.
     * @return void
     */
    public function isBadRequest($message, $errors = array()) {
        $this -> app -> response -> setStatus(400);
        $this -> respond($message, $errors);
    }

    /**
     * Creates a simple error response with HTTP code 404
     *
     * @param string $message Error message.
     * @return void
     */
    public function isNotFound($message) {
        $this -> app -> response -> setStatus(404);
        $this -> respond($message);
    }

    /**
     * Creates a simple error response with HTTP code 409
     *
     * @param string $message Error message.
     * @return void
     */
    public function isConflict($message) {
        $this -> app -> response -> setStatus(409);
        $this -> respond($message);
    }

    /**
     * Creates a simple error response with HTTP code 403
     *
     * @param string $message Error message.
     * @return void
     */
    public function isForbidden($message) {
        $this -> app -> response -> setStatus(403);
        $this -> respond($message);
    }

    /**
     * Creates a simple error response with HTTP code 500
     *
     * @param string $message Error message.
     * @return void
     */
    public function isInternalError($message) {
        $this -> app -> response -> setStatus(500);
        $this -> respond($message);
    }

    private function respond($message, $errors = array()) {
        $this -> app -> response -> headers -> set('Content-Type', 'application/json');
        $response = new stdClass();
        $response -> message = $message;
        if (!empty($errors)) {
            $response -> errors = $errors;
        }
        $this -> app -> response -> setBody(json_encode($response, JSON_PRETTY_PRINT));
    }

}
?>
