<?php
require '../api/rb.php';
require '../api/util/Valitron/Valitron.php';
require '../api/exception/ValidationException.php';
require '../api/util/Mail.php';
require '../api/model/user.php';
require '../api/util/Utilities.php';

$errorMessage = '';

if (version_compare(PHP_VERSION, '5.5.0') < 0) {
    $installationFailed = true;
    $errorMessage .= 'Die installierte PHP-Version ist kleiner als 5.6. OwnAntiTheft benötigt mindesten PHP 5.6!<br><br>';
}

if (!function_exists('curl_version')) {
    $installationFailed = true;
    $errorMessage .= 'cURL ist nicht installiert!<br><br>';
}

if (!extension_loaded('mbstring')) {
    $installationFailed = true;
    $errorMessage .= 'Multibyte String Erweiterung ist nicht installiert!';
}

if (!ini_get('allow_url_fopen')) {
    $installationFailed = true;
    $errorMessage .= 'allow_url_fopen ist nicht aktiviert!<br><br>';
}

if (!function_exists('mail')) {
    $installationFailed = true;
    $errorMessage .= 'OwnAntiTheft kann keine E-Mails versenden! Aktiviere die mail-Funktion.<br><br>';
}

if (!is_writeable(__FILE__)) {
    $installationFailed = true;
    $errorMessage .= 'Die Datei "install.php" hat keine Schreibrechte!<br><br>';
}

if (!is_writeable('../api')) {
    $installationFailed = true;
    $errorMessage .= 'Das Verzeichnis "api" ist nicht beschreibbar!<br><br>';
}

if (isset($_POST['email']) && !isset($installationFailed)) {

    $installationFailed = false;

    if ($_POST['dbtype'] === 'sqlite') {
        $_POST['dbname'] = $_POST['dbname'] . '.sqlite';
        R::setup('sqlite' . ':' . '../api/' . '/' . $_POST['dbname']);
    } else {
        R::setup($_POST['dbtype'] . ':host=' . $_POST['dbhost'] . ';port=' . $_POST['dbport'] . ';dbname=' . $_POST['dbname'], $_POST['dbuser'], $_POST['dbpw']);
    }

    if (R::testConnection()) {

        $template = file_get_contents('./OATSettingsTemplate.php');

        $template = str_replace('%%DBTYPE%%', $_POST['dbtype'], $template);
        $template = str_replace('%%DBHOST%%', $_POST['dbhost'], $template);
        $template = str_replace('%%DBPORT%%', $_POST['dbport'], $template);
        $template = str_replace('%%DBNAME%%', $_POST['dbname'], $template);
        $template = str_replace('%%DBUSER%%', $_POST['dbuser'], $template);
        $template = str_replace('%%DBPASSWORD%%', $_POST['dbpw'], $template);

        if (file_put_contents('../api/OATSettings.php', $template) === FALSE) {
            $installationFailed = true;
            $errorMessage = 'Schreiben der Einstellungsdatei fehlgeschlagen!';
        } else {

            require '../api/OATSettings.php';

            $config = new stdClass();
            $config -> email = $_POST['email'];
            $config -> password = generatePassword(8);
            $config -> api_url = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['SERVER_NAME'] . str_replace('/install/install.php', '/api', $_SERVER['SCRIPT_NAME']);
            $config -> sender_id = GOOGLE_SENDER_ID;
            $configEnc = base64url_encode(json_encode($config));

            $mail = new Mail();

            R::begin();
            try {
                //Initilize database
                if ($_POST['dbtype'] === 'sqlite') {
                    $sql = file_get_contents(__DIR__ . '/sqlite_db.sql');

                    $queries = explode(';', $sql);

                    foreach ($queries as $query) {
                        R::exec($query);
                    }
                } else {
                    $sql = file_get_contents(__DIR__ . '/db.sql');

                    R::exec($sql);
                }

                //Create admin user
                $user = R::dispense('user');
                $user -> email = strtolower($config -> email);
                $user -> password = password_hash($config -> password, PASSWORD_BCRYPT);
                $user -> isAdmin = true;
                R::store($user);

                //Send credentials to administrator
                $message = sprintf(NEW_USER_MAIL_TEMPLATE, $user -> email, $config -> password, $configEnc);
                $mail -> sendMail($user -> email, 'Ihre Zugangsdaten für OwnAntiTheft', $message);
                //Delete installation files
                // unlink(__DIR__ . '/db.sql');
                // unlink(__FILE__);
            } catch (ValidationException $e) {
                R::rollback();
                $installationFailed = true;
                $errorMessage = 'Invalide Eingabe!';
            } catch (MailException $e) {
                R::rollback();
                $installationFailed = true;
                $errorMessage = 'E-Mail konnte nicht gesendet werden!';
            } catch (\RedBeanPHP\RedException\SQL $e) {
                R::rollback();
                $installationFailed = true;
                print($e -> getMessage());
                $errorMessage = 'Es ist ein Fehler während der Datenbankinstallation aufgetreten!';
            } catch(Exception $e) {
                R::rollback();
                $installationFailed = true;
                $errorMessage = 'Unbekannter Fehler aufgetreten! ' . $e -> getMessage();
            }
        }
    } else {
        $installationFailed = true;
        $errorMessage = 'Datenbankverbindung konnte nicht aufgebaut werden!';
    }
}
?>

<!doctype html>
<html lang="de">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <base href="<?php echo str_replace('/install/install.php', '/', $_SERVER['SCRIPT_NAME']); ?>" />

        <title>OwnAntiTheft - Installation</title>
        <!-- STYLES -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/rdash.min.css">
        <link rel="stylesheet" type="text/css" href="css/oat.css">

    </head>
    <body>
        <div id="page-wrapper">
            <div id="content-wrapper">
                <div class="page-content">
                    <!-- Main Content -->
                    <div class="container" id="login-container">
                        <form class="form-horizontal" role="form" name="installationForm" method="post" action="install/install.php">
                            <fieldset>
                                <legend>
                                    Installation
                                </legend>
                                <?php
                                if (isset($installationFailed)) {
                                    if ($installationFailed === true) {
                                        echo "<div class='alert alert-danger'><span>$errorMessage</span></div>";
                                    } else {
                                        echo "<div class='alert alert-success'><span>Installation erfolgreich!</span></div>";
                                    }
                                }
                                ?>
                                <div class="form-group">
                                    <label for="sel1">Datenbanktyp</label>
                                    <select class="form-control" id="sel1" name="dbtype">
                                        <option value="mysql" selected="true">MySQL</option>
                                        <option value="sqlite">SQLite</option>
                                        <option value="pgsql">PostgreSQL</option>
                                        <option value="cubrid">CUBRID</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="dbname">Datenbank-Name</label>
                                    <input type="text" class="form-control" name="dbname" required/>
                                </div>
                                <div class="form-group">
                                    <label for="dbhost">Datenbank-Host</label>
                                    <input type="text" class="form-control" name="dbhost" />
                                </div>
                                <div class="form-group">
                                    <label for="dbport">Datenbank-Port</label>
                                    <input type="number" class="form-control" name="dbport" placeholder="3306"/>
                                </div>
                                <div class="form-group">
                                    <label for="dbuser">Datenbank-Nutzer</label>
                                    <input type="text" class="form-control" name="dbuser" />
                                </div>
                                <div class="form-group">
                                    <label for="dbpw">Datenbank-Passwort</label>
                                    <input type="password" class="form-control" name="dbpw" />
                                </div>
                                <hr class="featurette-divider">
                                <div class="form-group">
                                    <label for="email">E-Mail-Adresse des Administrators</label>
                                    <input type="email" class="form-control" name="email" placeholder="mustermann@beispiel.de" required />
                                </div>
                                <button class="form-control btn btn-primary" type="submit">
                                    Installation starten
                                </button>
                            </fieldset>
                        </form>
                    </div>
                </div><!-- End Page Content -->
            </div><!-- End Content Wrapper -->
        </div><!-- End Page Wrapper -->
    </body>
</html>
