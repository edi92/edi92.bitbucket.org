define({ "api": [
  {
    "type": "post",
    "url": "/devices/:aid/contacts-to-owner Send an e-mail with a backup of",
    "title": "his contacts",
    "version": "1.0.0",
    "name": "ContactsToOwner",
    "group": "Devices",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Authentication.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "description": "<p>text/x-vcard is required!</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "aid",
            "description": "<p>Android Device ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Updated-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 401 Bad Request\n{\n  \"message\": \"Only *.vcf files will be accepted.\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if device was not found:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Device not found!\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if you are not the device owner:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"message\": \"You are not the device owner!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/devices.php",
    "groupTitle": "Devices"
  },
  {
    "type": "post",
    "url": "/devices/:aid/command/:cno",
    "title": "Send a command to device",
    "version": "1.0.0",
    "name": "DeviceCommand",
    "group": "Devices",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Authentication</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "aid",
            "description": "<p>Android Device ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cno",
            "description": "<p>Command number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": true,
            "field": "data",
            "description": "<p>Additional parameter in request body, depending on command number.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Alarm (CNO=2) Request-Example:",
          "content": "{\n  \"duration\": 5000,\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Updated-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ObjectProperty",
            "description": "<p>Property which is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ErrorDescription",
            "description": "<p>Describes the error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 401 Bad Request\n{\n  \"message\": \"Invalid request!\"\n  \"errors\": {\n    \"ObjectProperty\": [\n      \"ErrorDescription\"\n    ]\n  }\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if device was not found:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Device not found!\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if you are not the device owner:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"message\": \"You are not the device owner!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/devices.php",
    "groupTitle": "Devices"
  },
  {
    "type": "get",
    "url": "/devices/:aid",
    "title": "Get device information",
    "version": "1.0.0",
    "name": "GetDevice",
    "group": "Devices",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Authentication</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "aid",
            "description": "<p>Android Device ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "aid",
            "description": "<p>Android Device ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the device.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "imei",
            "description": "<p>Device IMEI. 0 stands for devices without GSM module.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "battery_level",
            "description": "<p>Battery level (integer from 0 to 100).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Geopraphical longitude.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Geopraphical latitude.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "range",
            "description": "<p>Range of the current position.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "modified",
            "description": "<p>UNIX timestamp of the last modification.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "google_reg",
            "description": "<p>Google cloud messaging register id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "settings",
            "description": "<p>Settings object.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "settings.network_type",
            "description": "<p>Type of the network (0=not connected, 1=2G, 2=3G, 3=4G, 4=wifi).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "settings.brightness",
            "description": "<p>Display brightness (0 to 255).</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "settings.wifi",
            "description": "<p>Flag wheather wifi is toggled on or off.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "settings.gps",
            "description": "<p>Flag wheather gps is toggled on or off.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "settings.bt",
            "description": "<p>Flag wheather bluetooth is toggled on or off.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "settings.camera",
            "description": "<p>Flag wheather camera is toggled on or off.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "settings.sync",
            "description": "<p>Flag wheather the master synchronisation is toggled on or off.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "settings.locked",
            "description": "<p>Flag wheather the device is locked.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "settings.lock_message",
            "description": "<p>Message which will displayed on lockscreen.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "settings.system_app",
            "description": "<p>Flag wheather OwnAntiTheft is installed as system app.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "settings.device_admin",
            "description": "<p>Flag wheather OwnAntiTheft is configured as device admin.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "settings.phoneno",
            "description": "<p>Phone number for calling feature in locked mode.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n    {\n      \"aid\": \"994988ec4bde1de8\",\n      \"name\": \"A1-810\",\n      \"imei\": \"0\",\n      \"battery_level\": 26,\n      \"lng\": 0,\n      \"lat\": 0,\n      \"range\": 0,\n      \"modified\": 1449527548,\n      \"google_reg\":\n\"APA91bFanS64vpGK0vvdf7CYOi-U0bCfuGMjitCWmt7cFqCTedopnOOz9Rad3zkqPGe_IRDGUjKwo99okD_Ej5Ke01P_1Z60mcWHwiBaUCQG4E6r2uf6LuXHHCRKNGR0Keg08Gc7t502\",\n      \"settings\": {\n         \"wifi\": true,\n         \"gps\": true,\n         \"bt\": false,\n         \"brightness\": 40,\n         \"network_type\": 4,\n         \"locked\": false,\n         \"lock_message\": \"Test123\",\n         \"sync\": true,\n         \"camera\": true,\n         \"system_app\": false,\n         \"device_admin\": true,\n         \"phoneno\": \"49151209684393\"\n      }\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response if device was not found:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Device not found!\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if you are not the device owner:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"message\": \"You are not the device owner!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/devices.php",
    "groupTitle": "Devices"
  },
  {
    "type": "post",
    "url": "/devices/:aid/message-to-owner",
    "title": "Send an e-mail to owner",
    "version": "1.0.0",
    "name": "MessageToOwner",
    "group": "Devices",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Authentication</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "aid",
            "description": "<p>Android Device ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "subject",
            "description": "<p>Subject of the e-mail,</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>HTML-Message-Body</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "    {\n      \"subject\": \"SIM-Karten-Wechsel\",\n      \"message\": \"<html><body>Hallo,<br/> auf deinem Gerät wurde die SIM\ngewechselt.</body></html>\"\n    }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Updated-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response if device was not found:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Device not found!\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if you are not the device owner:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"message\": \"You are not the device owner!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/devices.php",
    "groupTitle": "Devices"
  },
  {
    "type": "delete",
    "url": "/devices/:aid",
    "title": "Remove device",
    "version": "1.0.0",
    "name": "RemoveDevice",
    "group": "Devices",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Authentication</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "aid",
            "description": "<p>Android Device ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response if device was not found:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Device not found!\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if you are not the device owner:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"message\": \"You are not the device owner!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/devices.php",
    "groupTitle": "Devices"
  },
  {
    "type": "post",
    "url": "/devices/:aid/remove-confirmation",
    "title": "Send remove confirmation",
    "version": "1.0.0",
    "name": "SendRemoveConfirmation",
    "group": "Devices",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Authentication</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "aid",
            "description": "<p>Android Device ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Updated-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response if device was not found:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Device not found!\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if you are not the device owner:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"message\": \"You are not the device owner!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/devices.php",
    "groupTitle": "Devices"
  },
  {
    "type": "post",
    "url": "/devices/:aid",
    "title": "Create and update device",
    "version": "1.0.0",
    "name": "UpdateDevice",
    "group": "Devices",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Authentication</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "aid",
            "description": "<p>Android Device ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the device.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "imei",
            "description": "<p>Device IMEI. 0 stands for devices without GSM module.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "battery_level",
            "description": "<p>Battery level (integer from 0 to 100).</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "cell_info",
            "description": "<p>Object which contains information about the current cell.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cell_info.cid",
            "description": "<p>Cell id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cell_info.lac",
            "description": "<p>Location area code</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cell_info.mcc",
            "description": "<p>mobile country code</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "cell_info.mnc",
            "description": "<p>mobile networt code</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "google_reg",
            "description": "<p>Google cloud messaging register id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "settings",
            "description": "<p>Settings object.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "settings.network_type",
            "description": "<p>Type of the network (0=not connected, 1=2G, 2=3G, 3=4G, 4=wifi).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "settings.brightness",
            "description": "<p>Display brightness (0 to 255).</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "settings.wifi",
            "description": "<p>Flag wheather wifi is toggled on or off.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "settings.gps",
            "description": "<p>Flag wheather gps is toggled on or off.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "settings.bt",
            "description": "<p>Flag wheather bluetooth is toggled on or off.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "settings.camera",
            "description": "<p>Flag wheather camera is toggled on or off.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "settings.sync",
            "description": "<p>Flag wheather the master synchronisation is toggled on or off.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "settings.locked",
            "description": "<p>Flag wheather the device is locked.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "settings.system_app",
            "description": "<p>Flag wheather OwnAntiTheft is installed as system app.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "settings.device_admin",
            "description": "<p>Flag wheather OwnAntiTheft is configured as device admin.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Example-Request:",
          "content": " {\n   \"battery_level\":67,\n   \"cell_info\":{\n     \"cid\":13321224,\n     \"lac\":18,\n     \"mcc\":262,\n     \"mnc\":3\n   },\n  \"imei\":\"355179062596378\",\n  \"name\":\"Fake-Device\",\n  \"google_reg\":\"APB92xX_xXX6hYo_XTkVwW8rpq7plC_M4GQJ9AL4nmm4AJjzFmGi4J-eK_c8p-efleTiTHcuUyQHfoy_5t4YVioBi4HAD_TFTz07i7cA-EGJuyWhWvQi9Yv0ycBeVOnwX9rF3VPCOH9w\",\n  \"settings\":{\n    \"brightness\":54,\n    \"bt\":true,\n    \"camera\":true,\n    \"gps\":false,\n    \"device_admin\":true,\n    \"system_app\":false,\n    \"locked\":false,\n    \"network_type\":4,\n    \"sync\":true,\n    \"wifi\":true\n  }\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Created-Response:",
          "content": "HTTP/1.1 201 Created",
          "type": "json"
        },
        {
          "title": "Updated-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ObjectProperty",
            "description": "<p>Property which is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ErrorDescription",
            "description": "<p>Describes the error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 401 Bad Request\n{\n  \"message\": \"Invalid request!\"\n  \"errors\": {\n    \"ObjectProperty\": [\n      \"ErrorDescription\"\n    ]\n  }\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if you are not the device owner:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"message\": \"You are not the device owner!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/devices.php",
    "groupTitle": "Devices"
  },
  {
    "type": "post",
    "url": "/devices/:aid/location",
    "title": "Update device location",
    "version": "1.0.0",
    "name": "UpdateDeviceLocation",
    "group": "Devices",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Authentication</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "aid",
            "description": "<p>Android Device ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lat",
            "description": "<p>Geographical latitude</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "lng",
            "description": "<p>Geographical longitude</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "accuracy",
            "description": "<p>Accuracy of the measurement</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"lat\": 13.4852,\n  \"lng\": 14.4582,\n  \"accuracy\": 14\n }",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Updated-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ObjectProperty",
            "description": "<p>Property which is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ErrorDescription",
            "description": "<p>Describes the error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 401 Bad Request\n{\n  \"message\": \"Invalid request!\"\n  \"errors\": {\n    \"ObjectProperty\": [\n      \"ErrorDescription\"\n    ]\n  }\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if device was not found:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"Device not found!\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if you are not the device owner:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"message\": \"You are not the device owner!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/devices.php",
    "groupTitle": "Devices"
  },
  {
    "type": "post",
    "url": "/users",
    "title": "Create a new user",
    "version": "1.0.0",
    "name": "CreateUser",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Authentication</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>E-mail address of the new user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"email\": \"test@byom.de\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 Created",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ObjectProperty",
            "description": "<p>Property which is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ErrorDescription",
            "description": "<p>Describes the error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 401 Bad Request\n{\n  \"message\": \"Invalid request!\"\n  \"errors\": {\n    \"ObjectProperty\": [\n      \"ErrorDescription\"\n    ]\n  }\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if you have no admin rights:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"message\": \"You are not authorized to add a new user!\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if there is already a user with the given",
          "content": "e-mail:\n    HTTP/1.1 409 Conflict\n    {\n      \"message\": \"Integrity constraint violation: email address already in\nuse!\"\n    }",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/users.php",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "/users/forgot-password",
    "title": "User forgot password",
    "version": "1.0.0",
    "name": "ForgotPassword",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>E-mail address of the new user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"email\": \"test@byom.de\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 401 Bad Request\n{\n  \"message\": \"Can not read input!\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if user was not found by email:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"User not found!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/users.php",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "/users/:uid/forgot-password/:token",
    "title": "User reset password",
    "version": "1.0.0",
    "name": "ResetPassword",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "uid",
            "description": "<p>Unique user id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>Password token from email. *</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ErrorMessage",
            "description": "<p>Message which describes the error.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 401 Bad Request\n{\n  \"message\": \"ErrorMessage\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response if user was not found by email:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"message\": \"User not found!\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/users.php",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "/users/whoami",
    "title": "Who am i",
    "version": "1.0.0",
    "name": "Whoami",
    "group": "Users",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "description": "<p>Basic Authentication</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"email\": \"test@byom.de\",\n  \"devices\": [\n    {\n      \"aid\": \"994988ec4bde1de8\",\n      \"name\": \"Device 1\",\n      \"link\": \"http://localhost/api/v1/994988ec4bde1de8\"\n    },\n    {\n      \"aid\": \"994988ec4bde1de9\",\n      \"name\": \"Device 2\",\n      \"link\": \"http://localhost/api/v1/994988ec4bde1de9\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "api/routes/users.php",
    "groupTitle": "Users"
  }
] });
