define({
  "name": "dialog-backend",
  "version": "1.0.0",
  "description": "API documentation of the Dia:Log backend.",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-02-11T19:05:26.506Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
