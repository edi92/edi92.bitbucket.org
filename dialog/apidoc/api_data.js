define({ "api": [
  {
    "type": "post",
    "url": "/food",
    "title": "Create new food",
    "name": "CreateFood",
    "group": "Food",
    "version": "0.0.0",
    "filename": "src/services/food.service.ts",
    "groupTitle": "Food",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-REALM-ACCESS-TOKEN",
            "description": "<p>Users unique access token.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorized",
            "description": "<p>The request was unauthorized.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "InvalidFoodDraft",
            "description": "<p>Food draft is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "InvalidFoodDraft.msg",
            "description": "<p>Error message.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String[]",
            "optional": false,
            "field": "InvalidFoodDraft.errors",
            "description": "<p>Individual field errors.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Unauthorized-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"type\": \"https://realm.io/docs/object-server/problems/invalid-credentials\",\n     \"title\": \"The provided credentials are invalid or the user does not exist.\",\n     \"status\": 401,\n     \"code\": 611\n}",
          "type": "json"
        },
        {
          "title": "InvalidFoodDraft-Response",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"msg\": \"Invalid food draft model.\",\n    \"errors\": [\n        {\n            \"value\": \"\",\n            \"property\": \"name\",\n            \"children\": [],\n            \"constraints\": {\n                \"isNotEmpty\": \"name should not be empty\"\n            }\n        },\n        {\n            \"value\": -1,\n            \"property\": \"carbs\",\n            \"children\": [],\n            \"constraints\": {\n                \"isPositive\": \"carbs must be a positive number\"\n            }\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Object",
            "optional": false,
            "field": "foodDraft",
            "description": "<p>A food draft.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "foodDraft.name",
            "description": "<p>Food name.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "foodDraft.carbs",
            "description": "<p>Amount of carbs included in food in carbohydrate units per 100g.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    \"name\": \"Test\",\n    \"carbs\": 10.00\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "food",
            "description": "<p>Food.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "food._id",
            "description": "<p>The food id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "food.name",
            "description": "<p>Name of the food.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "food.carbs",
            "description": "<p>Amount of carbs in carbohydrate units per 100g.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "food.createdAt",
            "description": "<p>ISO 8601 formatted date string.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "food.createdBy",
            "description": "<p>ID of the creator.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "food.__v",
            "description": "<p>Current version in database.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 Created\n{\n     \"_id\": \"5a0c1c7c21484b426bc33f41\",\n     \"name\": \"Test\",\n     \"carbs\": 5,\n     \"createdAt\": \"2018-02-11T12:56:44.036Z\",\n     \"createdBy\": \"__admin\",\n     \"__v\": 0\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "delete",
    "url": "/food/:id",
    "title": "Delete food",
    "name": "DeleteFood",
    "group": "Food",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "version": "0.0.0",
    "filename": "src/services/food.service.ts",
    "groupTitle": "Food",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Foods unique ID.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidId",
            "description": "<p>The <code>id</code> is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "FoodNotFound",
            "description": "<p>The <code>id</code> of the Food was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "PermissionError",
            "description": "<p>User has no admin permission.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorized",
            "description": "<p>The request was unauthorized.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "InvalidId-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"msg\": \"Invalid id.\"\n}",
          "type": "json"
        },
        {
          "title": "NotFound-Response:",
          "content": "HTTP/1.1 404 Not Found",
          "type": "json"
        },
        {
          "title": "PermissionError-Response",
          "content": "HTTP/1.1 403 Forbidden",
          "type": "json"
        },
        {
          "title": "Unauthorized-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"type\": \"https://realm.io/docs/object-server/problems/invalid-credentials\",\n     \"title\": \"The provided credentials are invalid or the user does not exist.\",\n     \"status\": 401,\n     \"code\": 611\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-REALM-ACCESS-TOKEN",
            "description": "<p>Users unique access token.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response",
          "content": "HTTP/1.1 204 No Content",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/food",
    "title": "Request all food information",
    "name": "GetAllFood",
    "group": "Food",
    "version": "0.0.0",
    "filename": "src/services/food.service.ts",
    "groupTitle": "Food",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-REALM-ACCESS-TOKEN",
            "description": "<p>Users unique access token.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorized",
            "description": "<p>The request was unauthorized.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "InvalidPagingParameter",
            "description": "<p>One or more of the given paging parameter is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "InvalidPagingParameter.msg",
            "description": "<p>Error message.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String[]",
            "optional": false,
            "field": "InvalidPagingParameter.errors",
            "description": "<p>Array of individual error messages.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Unauthorized-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"type\": \"https://realm.io/docs/object-server/problems/invalid-credentials\",\n     \"title\": \"The provided credentials are invalid or the user does not exist.\",\n     \"status\": 401,\n     \"code\": 611\n}",
          "type": "json"
        },
        {
          "title": "InvalidPagingParameter-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"msg\": \"Invalid query parameter.\",\n    \"errors\": [\n        \"Invalid sort query parameter.\"\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Paging": [
          {
            "group": "Paging",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>The page number.</p>"
          },
          {
            "group": "Paging",
            "type": "Number",
            "optional": true,
            "field": "limit",
            "description": "<p>The page limit/size.</p>"
          },
          {
            "group": "Paging",
            "type": "String",
            "optional": true,
            "field": "sort",
            "description": "<p>Name of the field to sort. A preceding '-' signals descending order.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "elements",
            "description": "<p>List of food elements.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "elements._id",
            "description": "<p>The food id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "elements.name",
            "description": "<p>Name of the food.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "elements.carbs",
            "description": "<p>Amount of carbs in carbohydrate units per 100g.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "elements.createdAt",
            "description": "<p>ISO 8601 formatted date string.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "elements.createdBy",
            "description": "<p>ID of the creator.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "elements.__v",
            "description": "<p>Current version in database.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "size",
            "description": "<p>Current page size.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>Current page number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "totalPages",
            "description": "<p>Total number of pages.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "totalElements",
            "description": "<p>Total number of found elements.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"elements\": [\n        {\n            \"_id\": \"5a0c1c7c21484b426bc33f41\",\n            \"name\": \"Test\",\n            \"carbs\": 5,\n            \"createdAt\": \"2018-02-11T12:56:44.036Z\",\n            \"createdBy\": \"__admin\",\n            \"__v\": 0\n        },\n        {\n            \"_id\": \"5a1532b1360971223ef2ef4f\",\n            \"name\": \"Test\",\n            \"carbs\": 3.5,\n            \"createdAt\": \"2018-02-11T12:56:44.036Z\",\n            \"createdBy\": \"__admin\",\n            \"__v\": 0\n        }\n    ],\n    \"size\": 20,\n    \"page\": 0,\n    \"totalElements\": 2,\n    \"totalPages\": 1\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/food/:id",
    "title": "Get food information",
    "name": "GetFood",
    "group": "Food",
    "version": "0.0.0",
    "filename": "src/services/food.service.ts",
    "groupTitle": "Food",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Foods unique ID.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidId",
            "description": "<p>The <code>id</code> is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "FoodNotFound",
            "description": "<p>The <code>id</code> of the Food was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorized",
            "description": "<p>The request was unauthorized.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "InvalidId-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"msg\": \"Invalid id.\"\n}",
          "type": "json"
        },
        {
          "title": "NotFound-Response:",
          "content": "HTTP/1.1 404 Not Found",
          "type": "json"
        },
        {
          "title": "Unauthorized-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"type\": \"https://realm.io/docs/object-server/problems/invalid-credentials\",\n     \"title\": \"The provided credentials are invalid or the user does not exist.\",\n     \"status\": 401,\n     \"code\": 611\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-REALM-ACCESS-TOKEN",
            "description": "<p>Users unique access token.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "food",
            "description": "<p>Food.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "food._id",
            "description": "<p>The food id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "food.name",
            "description": "<p>Name of the food.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "food.carbs",
            "description": "<p>Amount of carbs in carbohydrate units per 100g.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "food.createdAt",
            "description": "<p>ISO 8601 formatted date string.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "food.createdBy",
            "description": "<p>ID of the creator.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "food.__v",
            "description": "<p>Current version in database.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"_id\": \"5a0c1c7c21484b426bc33f41\",\n     \"name\": \"Test\",\n     \"carbs\": 2.35,\n     \"createdAt\": \"2018-02-11T12:56:44.036Z\",\n     \"createdBy\": \"__admin\",\n     \"__v\": 0\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/food/:id/image",
    "title": "Get food image",
    "name": "GetFoodImage",
    "group": "Food",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "image",
            "description": "<p>The food image as binary.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/services/food.service.ts",
    "groupTitle": "Food",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Foods unique ID.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidId",
            "description": "<p>The <code>id</code> is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "FoodNotFound",
            "description": "<p>The <code>id</code> of the Food was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorized",
            "description": "<p>The request was unauthorized.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "InvalidId-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"msg\": \"Invalid id.\"\n}",
          "type": "json"
        },
        {
          "title": "NotFound-Response:",
          "content": "HTTP/1.1 404 Not Found",
          "type": "json"
        },
        {
          "title": "Unauthorized-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"type\": \"https://realm.io/docs/object-server/problems/invalid-credentials\",\n     \"title\": \"The provided credentials are invalid or the user does not exist.\",\n     \"status\": 401,\n     \"code\": 611\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-REALM-ACCESS-TOKEN",
            "description": "<p>Users unique access token.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "Post",
    "url": "/food/:id/image",
    "title": "New food image",
    "name": "PostFoodImage",
    "group": "Food",
    "description": "<p>This is the endpoint to upload new food images. It requires a valid multipart form-data request. Uploading an image is an one-time operation. Uploading an image for foods which already have an image, will result in a conflict response. To perform this operation you have to be an admin user or the user who created the food.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "image",
            "description": "<p>The food image as binary.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/services/food.service.ts",
    "groupTitle": "Food",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Foods unique ID.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidId",
            "description": "<p>The <code>id</code> is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "FoodNotFound",
            "description": "<p>The <code>id</code> of the Food was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorized",
            "description": "<p>The request was unauthorized.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ConflictError",
            "description": "<p>Food already has an image.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "OwnerPermissionError",
            "description": "<p>The requesting user is neither admin nor the user tho created the food.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidContentTypeError",
            "description": "<p>The content type is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidFileError",
            "description": "<p>The files mime type is not supported.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "InvalidId-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"msg\": \"Invalid id.\"\n}",
          "type": "json"
        },
        {
          "title": "NotFound-Response:",
          "content": "HTTP/1.1 404 Not Found",
          "type": "json"
        },
        {
          "title": "Unauthorized-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"type\": \"https://realm.io/docs/object-server/problems/invalid-credentials\",\n     \"title\": \"The provided credentials are invalid or the user does not exist.\",\n     \"status\": 401,\n     \"code\": 611\n}",
          "type": "json"
        },
        {
          "title": "ConflictError-Response",
          "content": "HTTP/1.1 409 Conflict\n{\n    \"msg\": \"Food already has an image.\"\n}",
          "type": "json"
        },
        {
          "title": "OwnerPermissionError-Response",
          "content": "HTTP/1.1 403 Forbidden\n{\n    \"msg\": \"You are not authorized to upload an image for this food.\"\n}",
          "type": "json"
        }
      ]
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-REALM-ACCESS-TOKEN",
            "description": "<p>Users unique access token.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/food/search?search=:search",
    "title": "Search for food by name",
    "name": "SearchForFood",
    "group": "Food",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search",
            "description": "<p>Name of the food to search for.</p>"
          }
        ],
        "Paging": [
          {
            "group": "Paging",
            "type": "Number",
            "optional": true,
            "field": "page",
            "description": "<p>The page number.</p>"
          },
          {
            "group": "Paging",
            "type": "Number",
            "optional": true,
            "field": "limit",
            "description": "<p>The page limit/size.</p>"
          },
          {
            "group": "Paging",
            "type": "String",
            "optional": true,
            "field": "sort",
            "description": "<p>Name of the field to sort. A preceding '-' signals descending order.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "src/services/food.service.ts",
    "groupTitle": "Food",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "X-REALM-ACCESS-TOKEN",
            "description": "<p>Users unique access token.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorized",
            "description": "<p>The request was unauthorized.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "InvalidPagingParameter",
            "description": "<p>One or more of the given paging parameter is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "InvalidPagingParameter.msg",
            "description": "<p>Error message.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String[]",
            "optional": false,
            "field": "InvalidPagingParameter.errors",
            "description": "<p>Array of individual error messages.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Unauthorized-Response:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n     \"type\": \"https://realm.io/docs/object-server/problems/invalid-credentials\",\n     \"title\": \"The provided credentials are invalid or the user does not exist.\",\n     \"status\": 401,\n     \"code\": 611\n}",
          "type": "json"
        },
        {
          "title": "InvalidPagingParameter-Response:",
          "content": "HTTP/1.1 400 Bad Request\n{\n    \"msg\": \"Invalid query parameter.\",\n    \"errors\": [\n        \"Invalid sort query parameter.\"\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "elements",
            "description": "<p>List of food elements.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "elements._id",
            "description": "<p>The food id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "elements.name",
            "description": "<p>Name of the food.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "elements.carbs",
            "description": "<p>Amount of carbs in carbohydrate units per 100g.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "elements.createdAt",
            "description": "<p>ISO 8601 formatted date string.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "elements.createdBy",
            "description": "<p>ID of the creator.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "elements.__v",
            "description": "<p>Current version in database.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "size",
            "description": "<p>Current page size.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "page",
            "description": "<p>Current page number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "totalPages",
            "description": "<p>Total number of pages.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "totalElements",
            "description": "<p>Total number of found elements.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"elements\": [\n        {\n            \"_id\": \"5a0c1c7c21484b426bc33f41\",\n            \"name\": \"Test\",\n            \"carbs\": 5,\n            \"createdAt\": \"2018-02-11T12:56:44.036Z\",\n            \"createdBy\": \"__admin\",\n            \"__v\": 0\n        },\n        {\n            \"_id\": \"5a1532b1360971223ef2ef4f\",\n            \"name\": \"Test\",\n            \"carbs\": 3.5,\n            \"createdAt\": \"2018-02-11T12:56:44.036Z\",\n            \"createdBy\": \"__admin\",\n            \"__v\": 0\n        }\n    ],\n    \"size\": 20,\n    \"page\": 0,\n    \"totalElements\": 2,\n    \"totalPages\": 1\n}",
          "type": "json"
        }
      ]
    }
  }
] });
